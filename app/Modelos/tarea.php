<?php

    namespace Modelos;

    class Tarea extends Conexion{
        public $idTarea;
        public $nombreTarea;
        public $fechaD;
        public $fechaM;
        public $fechaA;
        public $descripcion;

        public function __construct(){
            parent::__construct();
        }
        public function llenar_post(){
            $this->nombreTarea = $_POST['nombre'];
            $this->fechaD = $_POST['fechaD'];
            $this->fechaM = $_POST['fechaM'];
            $this->fechaA = $_POST['fechaA'];
            $this->descripcion = $_POST['descripcion'];
            
        }
        public function crear(){
            $this->llenar_post();
            $pre = mysqli_prepare($this->con, "INSERT INTO tareas(nombre, fechaD, fechaM, fechaA, descripcion) VALUES (?,?,?,?,?)");
            $pre->bind_param("sssss", $this->nombreTarea, $this->fechaD, $this->fechaM, $this->fechaA, $this->descripcion);
            $pre->execute();
        }

        public function editar($idTarea){
            $this->llenar_post();
            $pre = mysqli_prepare($this->con, "UPDATE tareas SET nombre=?, fechaD=?, fechaM=?, fechaA=?, descripcion=?  WHERE id=?");
            $pre->bind_param("ssssss", $this->nombreTarea, $this->fechaD, $this->fechaM, $this->fechaA, $this->descripcion, $idTarea);
            $pre->execute();
        }

        public static function consultar($idTarea){
            $conexion = new Conexion;
            $pre = mysqli_prepare($conexion->con, "SELECT* FROM tareas WHERE id = ?");
            $pre->bind_param("s", $idTarea);
            $pre->execute();
            $resultado = $pre->get_result();
            return $resultado->fetch_object();
        }

        public static function consultarTodo(){
            $conexion = new Conexion;
            $pre = mysqli_prepare($conexion->con, "SELECT* FROM tareas");
            $pre->execute();
            $resultado = $pre->get_result();
            return $resultado;
        }

        public static function eliminar($idTarea){
            $conexion = new Conexion;
            $pre = mysqli_prepare($conexion->con, "DELETE FROM tareas WHERE id = ?");
            $pre->bind_param("s", $idTarea);
            $pre->execute();
        }
    }

?>