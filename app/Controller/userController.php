<?php
    include "app/Modelos/conexion.php";
    include "app/Modelos/tarea.php";
    use Modelos\Conexion;
    use Modelos\Tarea;

    class userController{
        public function consultar(){
            $tarea = Tarea::consultar($_POST['idTareas']);
            echo (json_encode($tarea));            
        }
        public function crear(){
            $tarea = new Tarea;
            $tarea->crear();
            echo("creado");
        }
        public function editar(){
            $tarea = new Tarea;
            $tarea->editar($_POST["idTareas"]);
            echo ("editado");
        }

        public function consultarTodo(){
            $tareas = Tarea::consultarTodo($_POST['idTareas']);
            $tarea;
            while($tarea = mysqli_fetch_array($tareas)){
                echo(json_encode($tarea));
            }            
        }

        public function eliminar(){
            Tarea::eliminar($_POST['idTareas']);
            echo "eliminar";         
        }
    }


?>